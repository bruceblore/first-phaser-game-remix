/*global Phaser*/

var game = new Phaser.Game(800, 600, Phaser.AUTO, '', null, false, false);
var game_state = {};
var dialogTimer, dialogTextLeft, dialogTextRight, loadText, background, player, satyanadella, portal, debug;
game_state.story_beforeGame = function () {};
game_state.story_beforeGame.prototype = {

    preload: function() {
		console.log("Welcome to my game!");
        game.load.spritesheet('MSCEO','assets/img/satyanadella.png', 400, 452);
        game.load.spritesheet('hero','assets/img/stickman.png', 26, 76);
        game.load.image('room','assets/img/genericroom.png');
        game.load.image('prison','assets/img/prison.png');
        game.load.image('portal','assets/img/portal.png');
        },

    create: function() {
        background = game.add.sprite(0, 0, 'room');
        player = game.add.sprite (387, 262, 'hero');
        player.frame = 5;
        player.animations.add ('left', [0, 1, 2, 3, 4], 10, true);
        player.animations.add ('right',[6, 7, 8, 9, 10],10, true);
        dialogTextLeft = game.add.text(16, 16, "", {font: '16px monospace', fill:'#0f0'});
        dialogTextRight = game.add.text(416, 16, "", {font: '16px monospace', fill:'#0f0'});
        dialogTimer = game.time.create(false);
        dialogTimer.add(0,   function(){dialogTextLeft.text = "Hi. I am Steve the Hacker Stickman.";}, this);
        dialogTimer.add(1400, function(){dialogTextLeft.text = "I program for eight hours every day.";}, this);
        dialogTimer.add(2840,function(){dialogTextLeft.text = "My favorite Linux distribution is Linux From Scratch.";}, this);
        dialogTimer.add(4960,function(){dialogTextLeft.text = "I am a good athlete and can jump high.";}, this);
        dialogTimer.add(6480,function(){dialogTextLeft.text = "I won't tell you whether that is because I exercise or just hacked into the\nbrowser and changed my properties.";}, this);
        dialogTimer.add(10920,function(){dialogTextLeft.text = "And I somehow have friends.";}, this);
        dialogTimer.add(12000,function(){dialogTextLeft.text = "I hate Microsoft, Apple, the Intel ME, and a single mistyped letter\nruining my code.";}, this);
        dialogTimer.add(15400,function(){
            dialogTextLeft.text = "";
            dialogTextRight.text= "(POLICE! OPEN UP!)";
        }, this);
        dialogTimer.add(16120, function(){
            dialogTextLeft.text = "What was that?";
            dialogTextRight.text= "";
        }, this);
        dialogTimer.add(16680, function(){
            background.destroy();
            game.stage.backgroundColor = 0x000000;
        }, this);
        dialogTimer.add(17680, function(){
            background = game.add.sprite(0, 0, 'prison');
            player = game.add.sprite(187, 262, 'hero');
            player.frame = 5;
        }, this);
        dialogTimer.add(18180, function(){
            dialogTextLeft = game.add.text(16, 16, "(Why am I in prison?)", {font: '16px monospace', fill:'#0f0'});
        }, this);
        dialogTimer.add(19020, function(){dialogTextLeft.text += "\n(Why can't I teleport to my house?)";}, this);
        dialogTimer.add(20420, function(){
            satyanadella = game.add.sprite(400, 74, 'MSCEO');
            satyanadella.frame = 0;
            dialogTextLeft.text = "";
            dialogTextRight = game.add.text(416, 16, "You have been imprisoned and your\nbackdoors removed because we suspect\nthat you would have prevented us from\nreaching our goals. You have 3 days\nto pay me 25 million bitcoin,", {fill:'#0f0', font: '16px monospace'});
        }, this);
        dialogTimer.add(27540, function(){dialogTextRight.text = "otherwise, we will run\n\"dd if=/dev/urandom of=/dev/sda\"\non your brain. Nice job making\nyourself a dependency of the game\nengine, by the way.That is why we did\nnot just delete you.";}, this);
        dialogTimer.add(35060, function(){
            dialogTextRight.text= "";
            dialogTextLeft.text = "Not fair. You can't imprison me for\nwhat I might do in the future. Also,\nthere can only ever be 21 million\nbitcoin.";
        }, this);
        dialogTimer.add(39780, function(){
            dialogTextLeft.text = "";
            dialogTextRight.text= "Loading response. Please wait...";
            satyanadella.frame = 1;
        }, this);
        dialogTimer.add(41060, function(){dialogTextLeft.text = "Hmm... I wonder what happens if I\nuncomment this one little\nline of code...";}, this);
        dialogTimer.add(44140, function(){
            dialogTextRight.text = "Welcome to the\nIntel Management Engine.\nEnter the backdoor password: ";
            portal = game.add.sprite(557, 257, 'portal');
        }, this);
        dialogTimer.add(46980, function(){
            dialogTextLeft.text = "I've got this!";
            dialogTextRight.text += 'a';
        }, this);
        dialogTimer.add(47180, function(){dialogTextRight.text += 'b';}, this);
        dialogTimer.add(47480, function(){dialogTextRight.text += 'c';}, this);
        dialogTimer.add(47680, function(){dialogTextRight.text += '1';}, this);
        dialogTimer.add(47880, function(){dialogTextRight.text += '2';}, this);
        dialogTimer.add(48080, function(){dialogTextRight.text += '3';}, this);
        dialogTimer.add(48280, function(){
            dialogTextLeft.text = "Here we go";
            player.destroy();
            player = game.add.sprite(587, 262, 'hero');
            player.frame = 5;
        }, this);
        dialogTimer.add(48680, function(){
            game.state.start('main');
        }, this);
        dialogTimer.start();
    },

    update: function() {
    }
};

window.setTimeout(function(){
    game.state.add('story_beforeGame', game_state.story_beforeGame);
    game.state.add('main', game_state.main);
    game.state.add('crashed-Microsoft', game_state.microsoft);
    game.state.add('story_beforeBosslevel', game_state.story_beforeBosslevel);
    game.state.add('bosslevel', game_state.bosslevel);
    game.state.add('winning', game_state.winning);
    game.state.start('story_beforeGame');
}, 500);

if (window.location.href.indexOf("debug") != -1) {
	alert("The game is running in debug mode.");
	document.addEventListener("keydown", function(evt){
	    game.state.add('story_beforeGame', game_state.story_beforeGame);
	    if(evt.keyCode == 49){
	        game.state.start("story_beforeGame");
		} else if(evt.keyCode == 50){
	        game.state.start("main");
	    } else if(evt.keyCode == 51){
	        game.state.start("story_beforeBosslevel");
	    } else if(evt.keyCode == 52){
	        game.state.start("bosslevel");
	    } else if(evt.keyCode == 53){
	        game.state.start("winning");
	    } else if(evt.keyCode == 48){
	        game.state.start("crashed-Microsoft");
	    }
	});
	debug = true;
} else {
	debug = false;
}