/*global Phaser game game_state*/
game_state.microsoft = function(){};
game_state.microsoft.prototype = {
    preload: function(){
        game.load.image('bluescreen', 'assets/img/bluescreen.png');
    },
    
    create: function(){
        game.camera.reset();
        game.add.sprite(0, 0, 'bluescreen');
    },
    
    update: function(){}
};