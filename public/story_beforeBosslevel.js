/*global Phaser game game_state, dialogTextLeft, dialogTextRight*/
game_state.story_beforeBosslevel = function(){};
game_state.story_beforeBosslevel.prototype = {
    preload: function(){
        game.load.spritesheet('player', 'assets/img/stickman.png', 26, 76);
        game.load.spritesheet('MSCEO', 'assets/img/satyanadella.png', 400, 452);
    },
    
    create: function(){
        try {
            dialogTextLeft = game.add.text(16, 16, "", {font: '16px monospace', fill:'#0f0'});
            dialogTextRight = game.add.text(416, 16, "", {font: '16px monospace', fill:'#0f0'});
            this.dialogTimer = game.time.create(true);
            this.dialogTimer.add(0, function(){
                this.player = game.add.sprite(10, 510, 'player');
                this.player.frame = 5;
                dialogTextLeft.text = "YES! I got into kernel space!";}, this);
            this.dialogTimer.add(1160, function(){dialogTextLeft.text = "I should be able to easily get rid of\nthe enemy!";}, this);
            this.dialogTimer.add(3080, function(){
                dialogTextLeft.text = "";
                dialogTextRight.text = "(Not so fast!)";
                this.MSCEO = game.add.sprite(200, 113, "MSCEO");
                this.MSCEO.frame = 0;
            }, this);
            this.dialogTimer.add(3640, function(){dialogTextRight.text = "I know how to use Spectre and\nMeltdown too!";}, this);
            this.dialogTimer.add(5360, function(){dialogTextRight.text = "In fact, I paid Intel to design them\ninto the chips!";}, this);
            this.dialogTimer.add(7440, function(){dialogTextRight.text = "You are going to /dev/null!";}, this);
            this.dialogTimer.add(8520, function(){
                game.state.start('bosslevel');
            }, this);
            this.dialogTimer.start();
        } catch (err) {
            alert(err);
        }
    },
    
    update: function(){}
};